import React from "react";
import { useTopMoviesQuery } from "./generated/graphql";

const App: React.FC = () => {
  const { data, loading, error } = useTopMoviesQuery();

  if (loading) {
    return <p>Loading ...</p>;
  }

  if (error) {
    return <p>{error.message}</p>;
  }

  if (!data) {
    return <p>was da los?</p>;
  }

  return (
    <main>
      <h1>Filme</h1>
      {data.allMovies.results.map(movie => (
        <section key={movie.id}>
          <h2>{movie.originalTitle}</h2>
          <ul>
            {movie.cast.map(c => (
              <li key={c.id}>{c.name}</li>
            ))}
          </ul>
        </section>
      ))}
    </main>
  );
};

export default App;
