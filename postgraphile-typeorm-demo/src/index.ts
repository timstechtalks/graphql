require('dotenv').config();
import * as path from 'path';
import 'reflect-metadata';
import { createApp, expressErrorHandler } from './lib/createApp';
import { postgraphile } from 'postgraphile';

import { createConnection } from 'typeorm';

async function main() {
  const app = createApp();

  const host = process.env['TYPEORM_HOST'];
  const username = process.env['TYPEORM_USERNAME'];
  const password = process.env['TYPEORM_PASSWORD'];
  const database = process.env['TYPEORM_DATABASE'];
  const databasePort = process.env['TYPEORM_PORT'] || 5432;

  const connection = await createConnection({
    type: 'postgres',
    host: process.env['TYPEORM_HOST'],
    username: process.env['TYPEORM_USERNAME'],
    password: process.env['TYPEORM_PASSWORD'],
    database: process.env['TYPEORM_DATABASE'],
    entities: [path.join(__dirname, 'entity/**')],
    migrations: [path.join(__dirname, 'migration/**')],
  });

  await connection.runMigrations();

  app.use(
    postgraphile(
      process.env.DATABASE_URL ||
        `postgres://${username}:${password}@${host}:${databasePort}/${database}`,
      'public',
      {
        watchPg: true,
        graphiql: true,
        enhanceGraphiql: true,
      },
    ),
  );

  app.use(expressErrorHandler);

  const expressServer = app.listen(process.env['PORT'], () => {
    const address = expressServer.address();
    if (typeof address === 'object') {
      const { address: host, port } = address;
      console.log('🚀 Listening on %s:%s', host, port);
    }
  });
}

main().catch(e => {
  console.error(e);
  process.exit(1);
});
