1. Auf [TMDB GraphQL](https://tmdb-graphql.com/) Queries zeigen
1. Introspection zeigen

---

1. Wenn der die API inspizieren kann und damit weiß wie sie aussieht, dann kann man daraus auch Code generieren
1. [GraphQL Codegen](https://graphql-code-generator.com/) online zeigen
1. Lokal zeigen
1. TopMovies Query schreiben
1. useTopMoviesQuery benutzen

---

1. apollo server oder postgraphile?
   - Brownfield Entwickler mit PostgreSQL da? Oder eher so die Greenfield Entwickler?



```graphql
query TopMovies($year: Int) {
  allMovies(year: $year) {
    results {
      id
      originalTitle
      genres {
        id
        name
      }
    }
  }
}
```

```bash
npm run gen
```

```typescript jsx
import React from "react";
import { useTopMoviesQuery } from "./generated/graphql";

const App: React.FC = () => {
  const { data, loading, error } = useTopMoviesQuery();

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>{error.message}</p>;
  }

  if (!data) {
    return <p>Not Found</p>;
  }

  return (
    <main>
      {data.allMovies.results.map(m => (
        <section key={m.id}>
          <h2>{m.originalTitle}</h2>
          <ul>
            {m.genres.map(g => (
              <li key={g.id}>{g.name}</li>
            ))}
          </ul>
        </section>
      ))}
    </main>
  );
};

export default App;
```
