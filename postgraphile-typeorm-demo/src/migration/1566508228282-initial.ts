import { MigrationInterface, QueryRunner } from 'typeorm';

export class initial1566508228282 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      // language=SQL
      `create schema app_public;`,
    );
    await queryRunner.query(
      // language=SQL
      `set search_path to app_public, public;`,
    );
    await queryRunner.query(
      // language=SQL
      `create table ponds
                 (
                     id   serial primary key,
                     name text not null
                 );
            `,
    );
    await queryRunner.query(
      // language=SQL
      `
                    create table fish
                    (
                        id      serial primary key,
                        pond_id int  not null references ponds,
                        name    text not null
                    );
            `,
    );
    await queryRunner.query(
      // language=SQL
      `
                    insert into ponds (name)
                    values ('Amy');`,
    );
    await queryRunner.query(
      // language=SQL
      `
                    insert into fish (pond_id, name)
                    values (1, 'Blub'),
                           (1, 'Bubble'),
                           (1, 'Guber');
            `,
    );
    await queryRunner.query(
      // language=SQL
      `
                    create table companies
                    (
                        id   serial primary key,
                        name text not null
                    );`,
    );
    await queryRunner.query(
      // language=SQL
      `
                    create table beverages
                    (
                        id             serial primary key,
                        company_id     int  not null references companies,
                        distributor_id int references companies,
                        name           text not null
                    );`,
    );
    await queryRunner.query(
      // language=SQL
      `
                    comment on constraint "beverages_distributor_id_fkey" on "beverages" is
                        E'@foreignFieldName distributedBeverages\n@foreignSimpleFieldName distributedBeveragesList';
            `,
    );
    await queryRunner.query(
      // language=SQL
      `
                    create table foo_genera
                    (
                        id   serial primary key,
                        name text not null
                    );`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      // language=sql
      `drop schema if exists app_public cascade;`,
    );
  }
}
