import { createError } from 'apollo-errors';

export const AuthorizationError = createError('AuthorizationError', {
  message: 'You are not authorized.',
});

export const AuthenticationError = createError('AuthenticationError', {
  message: 'You are not authenticated. Please log in.',
});

export const NotFoundError = createError('NotFoundError', {
  message: 'Not found',
});
