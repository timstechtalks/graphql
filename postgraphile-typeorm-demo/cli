#!/usr/bin/env bash

set -e

# General functions
BOLD_TEXT=$(tput bold)
NORMAL_TEXT=$(tput sgr0)
YELLOW='\033[1;33m' # Yellow text color
RED='\033[0;31m'    # Red text color
GREEN='\033[0;32m'    # Green text color
BLUE='\033[0;34m'    # Blue text color
NC='\033[0m'        # No text color

customPrint () {
    echo "######################################################"
    echo "# ${BOLD_TEXT}${@}"${NORMAL_TEXT}
    echo "######################################################"
}

printSuccess () {
   echo -e "${GREEN}${@}${NC}"
}

printInfo () {
   echo -e "${BLUE}${@}${NC}"
}

printWarning () {
   echo -e "${YELLOW}${@}${NC}"
}

printError () {
   echo -e "${RED}${@}${NC}"
}

printHelp () {
    echo "usage: cli [options] [flags]"
    echo "  options:"
    echo "    s|setup                       install dependencies and configure development environment"
    echo "      flags:"
    echo "        --help                    display this help"
    echo "        --clean                   clean interactively all files not tracked by git !CAUTION!"
    echo "        --force                   force file override"
    echo "        --debug                   enable debug mode for this script"
}

FORCE=NO
POSITIONAL=()
PRINT_HELP=NO

while [[ $# -gt 0 ]]; do
    key="$1"
    case ${key} in
        --help)
        PRINT_HELP=YES
        shift # past argument
        ;;

        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ ${PRINT_HELP} == "YES" ]; then
    printHelp
    exit
fi

case $1 in
    s|setup)
        bash ".development/setup.sh" ${@:2}
        ;;
    *)
        printHelp
        exit 1
esac